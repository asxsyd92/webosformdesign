
import { layer } from "@layui/layui-vue";

import webosutils from 'webosutils/webosutils';
import { h } from "vue";
class ListUtils {




    /**
     * 获取列表数据
     * @param config table配置文件
     * @param url 请求地址
     * @param data 参数
     */
    getList = (config: any, data: Object) => {
        //var i= layer.msg("加载中...", { icon : 16, time: 1000});
        config.value.loading = true;
        webosutils.http.post(config.value.api, data).then((res: any) => {
            config.value.loading = false;
            //layer.close(i);
            if (res.success) {
                config.value.data = res.data;

                if (config.value.page) {
                    config.value.page.total = res.count
                }

            }
        });

    }
    /**
     * 获取按钮
     * @param appid 路由地址：菜单id
     * @param config loading配置文件
     * @param buttons 按钮
     */
    getButton = (appid: any, config: any, buttons: any) => {
        return new Promise((resolve, reject) => {

            webosutils.http.post("/v1/api/appset/getRoleBuutton", { appid: appid }).then((res: any) => {


                if (res.success) {

                    buttons.value.toolbarbuttons = res.data.filter((item: any) => { return item.type == 1 });
                    buttons.value.rowbuttons = res.data.filter((item: any) => { return item.type == 2 });


                    var data = res.data.find((e: any) => e.events == "searchEvent");
                    var o = new Object() as any;
                    if (data == null) {
                        o.msg = "你没有查询权限";
                        o.success = false;
                        o.data = null;
                        config.value.loading = false;
                    } else {
                        if (data.api == '') {
                            o.msg = "请求未配置，请联系管路有";
                            o.success = false;
                            o.data = data;
                            config.value.loading = false;
                        } else {
                            o.msg = "获取成功";
                            o.success = true;
                            o.data = data;

                        }

                    }
                    resolve(o);
                }
            }).catch((res: any) => {
                layer.notify({
                    title: "Error",
                    content: "按钮获取失败，请刷新重试！",
                    icon: 2
                });
                config.value!.loading = false;
            });;
        });
    }

    //类别查询功能
    searchEvent = (config: any) => {

        if (config.value.page) {
            //点击搜索按钮时将当前页设置为1
            // config.value.page.current=1;
            let data = { ...config.value.search, page: config.value.page.current, limit: config.value.page.limit };
            this.getList(config, data);

        } else {
            this.getList(config, { ...config.value.search });
        }
    }

    /**
     * 自定义表单弹窗模块编辑功能
     * @param popform 弹窗组件
     * @param ent 事件
     * @param row 该行编辑数据数据Object
     * @param data 请求时需打开的数据包含回调函数
     * @param suppdata 补充的数据或者赋值的数据
     */
    editRowEvent = (popform: any, ent: any, row: any, data: object, suppdata: object) => {

        this.openform(ent.title, ent.animation, popform, data, [ent.areax, ent.areay], suppdata);


    }
    /**
    * 自定义表单弹窗模块克隆功能
    * @param popform 弹窗组件
    * @param ent 事件
    * @param row 该行编辑数据数据Object
    * @param data 请求时需打开的数据包含回调函数
    * @param suppdata 补充的数据或者赋值的数据
    */
    cloneRowEvent = (popform: any, ent: any, row: any, data: object, suppdata: object) => {

        this.openform(ent.name + "【" + row.title + "】" + "的配置", ent.animation, popform, data, [ent.areax, ent.areay], suppdata);




    }
    /**
    * 自定义表单弹窗模块新增功能
    * @param popform 弹窗组件
    * @param ent 事件
    * @param data 请求时需打开的数据包含回调函数
    * @param suppdata 补充的数据或者赋值的数据
    */

    addEvent = (popform: any, ent: any, data: object, suppdata: object) => {
        this.openform(ent.title, ent.animation, popform, data, [ent.areax, ent.areay], suppdata);



    }

    /**
     * 删除并更新列表列表
     * @param ent 事件
     * @param row 删除的行
     * @param search 搜索方法
     * @param config 配置
     */
    removeRowEvent = async (ent: any, row: any, searchfun: Function, config: any) => {

        let param = {};
        if (ent.param != null && ent.param != '') {
            try {
                param = JSON.parse(ent.param);
            } catch (e) {
                layer.notify({ title: "温馨提示", content: "配置的参数存在问题！" });
                return;

            }

        }
        layer.confirm("您确定要" + ent.name + "该数据",
            {
                btn:
                    [
                        {
                            text: '确认', callback: function (id: any) {
                                webosutils.http.post(ent.api, { id: row.id, ...param }).then((res: any) => {
                                    if (res.success) {
                                        layer.close(id);
                                        searchfun(config);
                                        layer.notify({ title: "温馨提示", content: res.msg })


                                    } else {
                                        layer.notify({ title: "温馨提示", content: res.msg })
                                        layer.close(id);
                                    }
                                }).catch((error: any) => {
                                    layer.notify({ title: "温馨提示", content: "网络错误！" })
                                    layer.close(id);
                                })

                            }
                        },
                        { text: '取消', callback: function (id: any) { layer.close(id); } }
                    ]
            })

    }
    /**
 * 多选事件
 * @param ent 事件
 * @param row 删除的行
 * @param search 搜索方法
 * @param config 配置
 */
    SelectEvent = async (ent: any, row: any, searchfun: Function, config: any) => {

        if (config.value.selectedKeys.length == 0) {
            layer.notify({ title: "温馨提示", content: "请选择要操作的数据！" });
            return;
        }
        let param = {} as any;
        if (ent.param != null && ent.param != '') {
            try {
                param = JSON.parse(ent.param);
            } catch (e) {
                layer.notify({ title: "温馨提示", content: "配置的参数存在问题！" });
                return;

            }

        }
        layer.confirm("您确定要" + ent.name + "该数据",
            {
                btn:
                    [
                        {
                            text: '确认', callback: function (id: any) {
                                if (param["download"] === "download") {
                                    window.location.href = ent.api + "?id=" + config.value.selectedKeys.join(",");
                                    layer.notify({ title: "温馨提示", content: "请等待下载完成！" })
                                    layer.close(id);
                                } else {
                                    webosutils.http.post(ent.api, { id: config.value.selectedKeys.join(","), ...param }).then((res: any) => {
                                        if (res.success) {
                                            layer.close(id);
                                            searchfun(config);
                                            layer.notify({ title: "温馨提示", content: res.msg })


                                        } else {
                                            layer.notify({ title: "温馨提示", content: res.msg })
                                            layer.close(id);
                                        }
                                    }).catch((error: any) => {
                                        layer.notify({ title: "温馨提示", content: "网络错误！" })
                                        layer.close(id);
                                    })
                                }


                            }
                        },
                        { text: '取消', callback: function (id: any) { layer.close(id); } }
                    ]
            })

    }
    previewEvent = async (popform: any, ent: any, row: any) => {
        this.previewform(ent.title, ent.animation, popform, { fromid: ent.formid, instanceid: row.id }, [ent.areax, ent.areay]);


    }

    /**
     * 没有确认按钮的表单弹窗
     * @param title 弹窗变态
     * @param anim 弹窗动画
     * @param form 自定义表单组件
     * @param data 包含表单id和数据组件id
     * @param area 弹窗大小
     */
    previewform = (title: string, anim: any, form: any, data: object, area: string[]) => {

        if (anim == null) {
            anim = 0;
        }
        layer.open({
            title: title,
            area: area,
            content: h(form, data),
            shade: true,
            anim: anim,
            shadeClose: false,
            btn: [
                {
                    text: "取消",
                    callback: (resp: any) => {
                        layer.close(resp);
                    },
                },
            ]
        })

    }

    /**
     * 打开表单
     * @param title 标题
      * @param anim 动画
       * @param form popform组件
      * @param data  fromid:fromid,instanceid:"",callback:Callback 
      * @param area 弹出大小
      * @param suppdata 补充数据提交是会填充data中提交给后台
      */
    openform = (title: string, anim: any, form: any, data: any, area: string[], suppdata: Object) => {
        var ks = h(form, data) as any;
        if (anim == null) {
            anim = 0;
        }
        layer.open({
            title: title,
            area: area,
            content: ks,
            shade: true,
            anim: anim,
            shadeClose: false,
            btn: [
                {
                    text: "确认",
                    callback: (resp: any) => {
                        ks.component.exposed.validate(suppdata, resp, layer);
                    },
                },
                {
                    text: "取消",
                    callback: (resp: any) => {
                        layer.close(resp);
                    },
                }]
        });
    }
    showwindow = (title: string, anim: any, form: any, data: any, area: string[],showtitle:number, taskbar: any) => {
        debugger

        var ks = h(form, {fromdata:data,fromvalue:{}, callback: () => {
            console.log('callback');
        }
        }) as any;
        if (anim == null) {
            anim = 0;
        }
        layer.open({
            title:showtitle==0?false:title,
            area: area,
            content: ks,
            shade: false,
            anim: anim,
            shadeClose: false,
            //  type: 1,
            maxmin: true,
            // btn: [

            //     {
            //         text: "关闭",
            //         callback: (resp: any) => {
            //             layer.close(resp);
            //                    //根据id移除taskbar在前端的显示
            //             taskbar.value = taskbar.value.filter((item: any) => item.id != resp);
            //         },
            //     }],
            min: (id: string) => {


            },
            full: (id: string) => {
        
                console.log(`最大化:${id}`)
            },
            revert: (id: string) => {
                //根据id更新taskbar中的isfull为false
        
                console.log(`还原:${id}`)
            },
            close: (id: string) => {
                //根据id移除taskbar在前端的显示
             //   taskbar.value = taskbar.value.filter((item: any) => item.id != id);
            },
            success: (id: string) => {


            }
        });
    }
    revert = (id: string) => {
        layer.revert(id);
    }
    full = (id: string) => {
        layer.full(id);
    }
    min = (id: string) => {
        layer.min(id);
    }
    getUrlParams = (url: string) => {
        const paramsRegex = /[?&]+([^=&]+)=([^&]*)/gi;
        const params = new Object() as any;
        let match;
        while (match = paramsRegex.exec(url)) {
            params[match[1]] = match[2];
        }
        return params;
    };
    WebSocketOpen = () => {

    }
    WebSocketClose = (event: any) => {
        // connected.value = false;
        this.Speak("服务关闭，请重连");
        this.Speak("服务已断开，5秒后，重新连接");
        setTimeout(() => {
            this.Speak("正在重新连接");
            this.WebSocketOpen();
        }, 5000);
    };

    WebSocketError = (error: any) => {

        this.Speak("服务已断开，5秒后，重新连接");
        setTimeout(() => {
            this.Speak("正在重新连接");
            this.WebSocketOpen();
        }, 5000);

    };
    Speak = (mgs: string) => {
        var msg = new SpeechSynthesisUtterance();
        msg.lang = 'zh-CN'; //设置语言
        msg.text = mgs;
        msg.volume = 100; //设置音量
        msg.rate = 1; //说话速度 
        msg.pitch = 1.5; //设置话语音调(值越大越尖锐,越低越低沉)

        // console.log(msg); 
        window.speechSynthesis.speak(msg);
    }

    WebSocketMessage = (event: any) => {
        const data = JSON.parse(event.data); // 假设服务器发送的是JSON格式的数据  

        if (data.type === 'notice') {
            this.Speak(data.data);

        };
    }
}
export default new ListUtils;

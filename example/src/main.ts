
import { createApp } from 'vue'


import Layuid from '@layui/layui-vue'
import App from './App.vue'
import '@layui/layui-vue/lib/index.css';
import 'font-awesome/css/font-awesome.min.css';
import { useI18n } from "@layui/layui-vue";
const app = createApp(App)
app.use(Layuid);

app.mount('#play');

  
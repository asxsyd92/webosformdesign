# webos-form-design（webos）v3.2

单独剥离vuewebos，需要安装layui-vue，并且设置全局。
框架依赖，需要自行安装，以下未打包到依赖

vue vuedraggable  moment  vue-ueditor-wrap  vue3-json-editor  webosutils  @layui/layui-vue  md-editor-v3  lodash
主要实现表单组件图片上传、颜色选择、按钮、文本输入框、下拉框、单选框、多选框、开关、日期选择、时间选择、文件上传、表格、树形结构、富文本编辑器。

## 安装

```
npm install webos-form-design
```

## 需安装其他依赖

```

npm i webosutils
npm i  vuedraggable
npm i moment 
npm i  vue-ueditor-wrap  
npm i vue3-json-editor 
npm i webosutils 
npm i @layui/layui-vue 
npm i md-editor-v3 
npm i lodash
```

## 引入

```

import { createApp } from 'vue'


import Layuid from '@layui/layui-vue'
import App from './App.vue'
import '@layui/layui-vue/lib/index.css';
import 'font-awesome/css/font-awesome.min.css';

const app = createApp(App)

app.use(Layuid);

app.mount('#app');
```

## 完整例子

```
<template>
    <!-- 弹窗表单 -->
<lay-button type="primary" @click="openform()">弹窗表单</lay-button>

<!-- 普通页面表单 -->
<PageForm :fromdata="fromdata" :fromvalue="{}"
     @callback="(e) => { 
        console.log(e);
    }" @errorcallback="() => {
        console.log('errorcallback');
    }
    ">

</PageForm> 
<!-- 表单设计器 -->
<FormDesign @preview="(e) => {

       utls.openform(LayerForm, e);
}
"></FormDesign>
</template>
<script setup lang="ts">
import { ref,h } from "vue";
import { layer } from "@layui/layer-vue";
import   'webos-form-design/lib/index/css';
import { LayerForm,PageForm,FormDesign } from "webos-form-design";
import {utls} from './utls/index';
const openform = () => {
    console.log(LayerForm);
    debugger;
    utls.openform(LayerForm, fromdata);

}
var fromdata = {
    form: {
        mode: "",
        name: "",
        style: "",
        table: "",
        type: "",
        url: "",
    }, data: [{
        icon: "fa fa-edit",
        name: "title",
        id: "1",
        type: "input",
        data: {
            id: "title",
            col: "12",
            label: "标题",
            type: "text",
            name: "title",
            autocomplete: "off",
            placeholder: "请输入标题",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            required: "true",
            value: "",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    },
    {
        icon: "fa fa-edit",
        name: "title2",
        id: "1",
        type: "input",
        data: {
            id: "title2",
            col: "12",
            label: "标题2",
            type: "text",
            name: "title2",
            autocomplete: "off",
            placeholder: "placeholder",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            value: "",
            required: "true",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    }
    ,
    {
        icon: "fa fa-edit",
        name: "title3",
        id: "1",
        type: "input",
        data: {
            id: "title3",
            col: "24",
            label: "标题2",
            type: "password",
            name: "title3",
            autocomplete: "off",
            placeholder: "placeholder",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            required: "true",
            value: "",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    }
],
 rules: {},
 field:{}
};



</script>
```

# 完整的表单Json结构

```{
 {
    form: {
        mode: "",
        name: "",
        style: "",
        table: "",
        type: "",
        url: "",
    }, data: [{
        icon: "fa fa-edit",
        name: "title",
        id: "1",
        type: "input",
        data: {
            id: "title",
            col: "12",
            label: "标题",
            type: "text",
            name: "title",
            autocomplete: "off",
            placeholder: "请输入标题",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            required: "true",
            value: "",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    },
    {
        icon: "fa fa-edit",
        name: "title2",
        id: "1",
        type: "input",
        data: {
            id: "title2",
            col: "12",
            label: "标题2",
            type: "text",
            name: "title2",
            autocomplete: "off",
            placeholder: "placeholder",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            value: "",
            required: "true",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    }
    ,
    {
        icon: "fa fa-edit",
        name: "title3",
        id: "1",
        type: "input",
        data: {
            id: "title3",
            col: "24",
            label: "标题2",
            type: "password",
            name: "title3",
            autocomplete: "off",
            placeholder: "placeholder",
            inputclass: "block",
            disabled: "block",
            showtext: "false",
            required: "true",
            value: "",
            data: { api: "", para: {} },
            input: "",
            excel: "false",
        }
    }
],
 rules: {},
 field:{}
};
```

# 



实例请查看example

npm  run dev

// types/LayerForm.d.ts
import { DefineComponent } from 'vue';

interface FormDesignProps {
  // 定义你的 props 类型
  // 例如: title: string;
}

interface FormDesignEmits {
  // 定义你的事件类型
  // 例如: (e: CustomEvent) => void;
}

const LayerForm: DefineComponent<{}, FormDesignProps, FormDesignEmits>;
export default LayerForm;
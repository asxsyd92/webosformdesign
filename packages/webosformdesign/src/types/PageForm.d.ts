// types/LayerForm.d.ts
import { DefineComponent } from 'vue';

interface PageFormProps {
  // 定义你的 props 类型
  // 例如: title: string;
}

interface PageFormEmits {
  // 定义你的事件类型
  // 例如: (e: CustomEvent) => void;
}

const LayerForm: DefineComponent<{}, PageFormProps, PageFormEmits>;
export default LayerForm;
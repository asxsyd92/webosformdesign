// types/LayerForm.d.ts
import { DefineComponent } from 'vue';

interface LayerFormProps {
  // 定义你的 props 类型
  // 例如: title: string;
}

interface LayerFormEmits {
  // 定义你的事件类型
  // 例如: (e: CustomEvent) => void;
}

const LayerForm: DefineComponent<{}, LayerFormProps, LayerFormEmits>;
export default LayerForm;
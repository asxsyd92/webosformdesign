

//弹窗预览组件
 import LayerForm from "./component/formpreview/layerform.vue";
 //页面预览组件
 import PageForm from "./component/formpreview/pageform.vue";
 //设计器
 import FormDesign from "./component/formdesign/index.vue";

export {
 PageForm,
 LayerForm,
  FormDesign,
};





import setnumber from './input/setnumber.vue';
import setinput from './input/setinput.vue';
import setdatepicker from './input/setdatepicker.vue';
import setcolor from './color/setcolor.vue';
import setform from './form/setform.vue';
import seticon from './icon/seticon.vue';
import setinputtable from './table/setinputtable.vue'; 
import setjsoneditor from './jsoneditor/setjsoneditor.vue';
import setornament from './ornament/setornament.vue';
import setqrcode from './qrcode/setqrcode.vue';
import setquote from './quote/setquote.vue';
import setradio from './radio/setradio.vue';
import setrate from './rate/setrate.vue';
import setselect from './select/setselect.vue';
import settip from './tips/settip.vue';
import settree from './tree/settree.vue';
import setcheckbox from './checkbox/setcheckbox.vue';
import setmd from './richtext/setmd.vue';
import settextarea from './input/settextarea.vue';
import setueditor from './richtext/setueditor.vue';
import setbaiduuplod from './richtext/setbaiduuplod.vue';
import setonlyoffice from './office/setonlyoffice.vue';
import setcascader from './cascader/setcascader.vue';
import setformcontainer from './form/setformcontainer.vue';
import setrules from './form/setrules.vue';
import setqrcode from './qrcode/setqrcode.vue';

export default { 
    setdatepicker,
    setinput,
    setnumber,
    setcolor,
    setform,
    seticon,
    setjsoneditor,
    setornament,
    setqrcode,
    setquote,
    setradio,
    setrate,
    setselect,
    settip,
    settree,
    setcheckbox,
    setmd,
    settextarea,
    setueditor,
    setbaiduuplod,
    setinputtable,
    setonlyoffice,
    setcascader,
    setformcontainer,
    setrules,

}
import designumber from './input/designumber.vue';
import designinput from './input/designinput.vue';
import designdatepicker from './input/designdatepicker.vue';
import designcolor from "./color/designcolor.vue";
import designcheckbox from './checkbox/designcheckbox.vue'; 
import designquote from './quote/designquote.vue';
import designradio from './radio/designradio.vue'; 
import designselect from './select/designselect.vue'; 
import designmd from './richtext/designmd.vue'; 
export default {
    designumber, 
    designdatepicker,
    designinput,
    designcolor,
    designradio,
    designselect,
    designcheckbox,
    designmd,
    
}
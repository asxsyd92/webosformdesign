import itemnumber from './input/itemnumber.vue';
import iteminput from './input/iteminput.vue';
import itemtextarea from './input/itemtextarea.vue';
import itemdatepicker from './input/itemdatepicker.vue';
import itemcolor from './color/itemcolor.vue';
import itemicon from './icon/itemicon.vue'; 
import itemradio from './radio/itemradio.vue';
import itemcheckbox from './checkbox/itemcheckbox.vue';
import md from "./richtext/md.vue";
import ueditor from "./richtext/ueditor.vue";
import baiduuplod from "./richtext/baiduuplod.vue";
import itemselect from './select/itemselect.vue';
import itemtree from './tree/itemtree.vue';
import iteminputtable from './table/iteminputtable.vue';
import onlyoffice from './office/onlyoffice.vue'; 
import itemquote from './quote/itemquote.vue';
import itemrate from './rate/itemrate.vue';
import itemcascader from './cascader/itemcascader.vue';
import itemqrcode from './qrcode/itemqrcode.vue';
import itemornament from './ornament/itemornament.vue';
import itemjsoneditor from './jsoneditor/itemjsoneditor.vue';
export default {
    itemnumber, 
    iteminput,
    itemtextarea,
    itemdatepicker,
    itemcolor,
    itemicon,
    itemradio,
    itemcheckbox,
    md,
    ueditor,
    baiduuplod,
    itemselect,
    itemtree,
    iteminputtable,
    onlyoffice,
    itemquote,
    itemrate,
    itemcascader,
    itemqrcode,itemornament,itemjsoneditor
}
 export interface FormType {
  mode: string;
  name: string;
  style: string;
  table: string;
  type: string;
  url: string;
}
///
/// @渲染表单组件
/// @author asxsyd92   asxsyd92.com
/// @version 1.0.0
/// @copyright Copyright (c) 2018 Verizon. All rights reserved.

const formPreview = {
  install(Vue, options) {
    Vue.component('form-preview', () => import('./formPreview.vue'))
  }
}

export default formPreview
const layerform = {
  install(Vue, options) {
    Vue.component('layer-form', () => import('./layerform.vue'))
  }
}

export default layerform
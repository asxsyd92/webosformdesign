
export interface FormType {
  form: Form;
  data: any[];
  rules: Rules;
  field: Object;
}


interface Rules {
}

interface Form {
  mode: string;
  name: string;
  style: string;
  table: string;
  type: string;
  url: string;
}
export interface ItemJsonEditor {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemJsonEditorData;
}

export interface ItemJsonEditorData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: ItemJsonEditorValue;
  data: ItemJsonEditorValue;
  input: string;
  excel: string;
}

export interface ItemJsonEditorValue {
}
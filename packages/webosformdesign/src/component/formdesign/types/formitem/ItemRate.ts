export interface ItemRate {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemRateData;
}

export interface ItemRateData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: string;
  data: string;
  input: string;
  excel: string;
}
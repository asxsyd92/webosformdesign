export interface ItemSelect {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemSelectData;
}

export interface ItemSelectData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: string;
  data: any;
  showSearch: string;
  multiple: string;
  input: ItemSelectInput[];
  excel: string;
}

export interface ItemSelectInput {
  title: string;
  value: string;
}
export interface ItemQrcode {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemQrcodeData;
}

export interface ItemQrcodeData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  display: string;
  value: string;
  data: ItemQrcodeDetails;
  input: string;
  excel: string;
}

export interface ItemQrcodeDetails {
  color: string;
  width: number;
  backgroundcolor: string;
  text: string;
  field: string;
  setfield: string;
  isshow: string;
  symbol: string;
}
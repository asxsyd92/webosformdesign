export interface ItemQuote {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemQuoteData;
}

export interface ItemQuoteData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  required: string;
  data: string;
  input: string;
  excel: string;
}
export interface ItemCheckbox {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemCheckboxData;
}

export interface ItemCheckboxData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: string;
  data: any;
  input: string;
  excel: string;
}
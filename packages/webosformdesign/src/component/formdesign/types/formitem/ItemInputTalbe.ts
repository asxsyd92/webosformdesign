export interface ItemInputTalbe {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemInputTalbeData;
}

export interface ItemInputTalbeData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: ItemInputTalbeValue;
  data: ItemInputTalbeValue;
  input: string;
  excel: string;
}
export interface ItemInputTalbeValue = {

};

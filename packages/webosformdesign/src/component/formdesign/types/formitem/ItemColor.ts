export interface ItemColor {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemColorData;
}

export interface ItemColorData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  data: string;
  input: string;
  excel: string;
    required: string;
}
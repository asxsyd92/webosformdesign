export interface ItemOrnament {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemOrnamentData;
}

export interface ItemOrnamentData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  display: string;
  value: string;
  data: string;
  input: string;
  excel: string;
}
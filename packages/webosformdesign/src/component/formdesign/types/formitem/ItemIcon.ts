export interface ItemIcon {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemIconData;
}

export interface ItemIconData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  data: string;
  input: string;
  excel: string;
}
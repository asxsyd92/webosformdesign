export interface ItemForm {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemFormData;
}

export interface ItemFormData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  value: string;
  data: string;
  input: string;
  excel: string;

}
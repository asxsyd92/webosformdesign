export interface ItemRadio {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemRadioData;
}

export interface ItemRadioData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  value: string;
  data: any;
  disabled: string;
  showtext: string;
  input: string;
  excel: string;
}
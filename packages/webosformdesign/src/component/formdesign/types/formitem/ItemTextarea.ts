export interface ItemTextarea {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemTextareaData;
}

export interface ItemTextareaData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  required: string;
  display: string;
  value: string;
  data: string;
  input: string;
  excel: string;
}
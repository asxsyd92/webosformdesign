export interface ItemBaiduUplod {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemBaiduUplodData;
}

export interface ItemBaiduUplodData  {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  data: number;
  input: string;
  excel: string;  
  required: string;
}

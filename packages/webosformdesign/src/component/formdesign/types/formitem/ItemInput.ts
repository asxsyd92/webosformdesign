export interface ItemInput {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemInputData;
}

export interface ItemInputData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  data: any;
  input: string;
  excel: string;
      required: string;
}


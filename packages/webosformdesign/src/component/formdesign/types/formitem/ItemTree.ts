export interface ItemTree {
  icon: string;
  name: string;
  id: string;
  type: string;
  data: ItemTreeData;
}

export interface ItemTreeData {
  id: string;
  col: string;
  label: string;
  type: string;
  name: string;
  autocomplete: string;
  placeholder: string;
  inputclass: string;
  disabled: string;
  showtext: string;
  value: string;
  required: string;
  data: string;
  input: string;
  excel: string;
  checkstrictly: string;
}
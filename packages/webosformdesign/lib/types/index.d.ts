
// components.d.ts

// 声明 Vue 组件类型（这些类型通常由 Vue SFC 编译器自动生成，但这里为了示例而手动声明）
declare module '*.vue' {
  import { DefineComponent } from 'vue';
  const component: DefineComponent<{}, {}, any>;
  export default component;
}



// 由于 Vue 组件本身不需要在这里声明（它们由 .vue 文件和 Vue SFC 编译器处理），
// 我们只需要声明任何额外的、非自动生成的类型。
// 如果 LayerForm, PageForm, FormDesign 是类型而不是组件，那么它们应该在这里声明，
// 但由于它们是组件，并且通常作为 Vue 组件导入，所以它们不需要在这里声明。